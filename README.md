# tags
Change tags of audio files, automatically or interactively.

## About
Change title, artist, album tags of audio files automatically or Manually input values for each tag  
If file name is `Some song name - Some Artist - Some Album.mp3` then it will tag the file with title as `Some song name`, artist as `Some Artist`, album as `Some Album`.  
It uses ` - ` as separator, file name should be in form of `Title - Artist - Album.mp3`  
If `Album` is not present in name, it will use `Title` as album name.  
If `Artist` is not present in name, it won't update artist tag (only `Title` and `Album` tags will be updated)  

## Installation
Its just a python script
download it, make it executable and put somewhere in your $PATH

#### with wget
``` bash
wget https://codeberg.org/anhsirk0/tags/raw/branch/main/tags.py -O tags
```
#### or with curl
``` bash
curl https://codeberg.org/anhsirk0/tags/raw/branch/main/tags.py --output tags
```
#### making it executable
```bash
chmod +x tags
```
#### copying it to somewhere in $PATH
```bash
cp tags ~/.local/bin/
```
or 
```bash
cp tags /usr/local/bin/    # root required
```

## Dependencies
[python-mutagen](https://mutagen.readthedocs.io/en/latest/) is the only dependency  
If you are on linux its probably already installed  
For Arch based
```bash
sudo pacman -S python-mutagen
```

For Debian based
```bash
sudo apt install python3-mutagen
```

With PIP
```bash
pip install mutagen
```

## Usage
```text
usage: tags.py [-h] [-i] [-d] FILE [FILE ...]

positional arguments:
  FILE               Files to change tags of

options:
  -h, --help         show this help message and exit
  -i, --interactive  Input tags interactively
  -d, --dry-run      Show what will happen
```
